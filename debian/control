Source: scummvm-tools
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Stephen Kitt <skitt@debian.org>
Section: games
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libboost-program-options-dev,
               libflac-dev,
               libfreetype-dev,
               libmad0-dev,
               libpng-dev,
               libvorbis-dev,
               libwxgtk3.2-dev,
               pkgconf,
               zlib1g-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/games-team/scummvm-tools
Vcs-Git: https://salsa.debian.org/games-team/scummvm-tools.git
Homepage: https://wiki.scummvm.org/index.php?title=ScummVM_Tools
Rules-Requires-Root: no

Package: scummvm-tools
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: collection of tools for ScummVM
 This package contains various tools which may be useful in
 conjunction with ScummVM.
 .
 The tools include:
  * compression tools to re-compress ScummVM games' assets;
  * extraction tools, either to extract resources from game assets, or
    to convert assets from one format to another (e.g. PC Engine or
    Macintosh releases);
  * game script analysis tools.
 .
 A GUI is provided alongside the command-line tools.
